import QtQuick 2.0

Rectangle {
    property alias icon: buttonImage.source
    property string buttonType: ""
    id: sideMenuButton
    width: parent.width * 0.75
    height: width
    color: "yellow"
    anchors.horizontalCenter: parent.horizontalCenter
    border {
        color: "orange"
        width: 4
    }

    Image {
        id: buttonImage
        width: parent.width-20
        height: parent.height-20
        fillMode: Image.PreserveAspectFit
        anchors.centerIn: parent
    }

    MouseArea {
        anchors.fill: parent
        hoverEnabled: true

        onEntered: {
            sideMenuButton.border.color = "red"
            sideMenuButton.color = "orange"
        }

        onClicked: {
            if (buttonType == "quit_game")
                game.quitGame();
            if (buttonType == "save_file")
                game.saveToFile();
            if (buttonType == "load_file"){
                game.loadFromFile("C:/Users/mrlat/townbuilder-game/townbuilder/test_file.xml");

            }            
            game.setCurrentToolQString(buttonType)


        }

        onExited: {
            sideMenuButton.border.color = "orange"
            sideMenuButton.color = "yellow"
        }

    }

}
