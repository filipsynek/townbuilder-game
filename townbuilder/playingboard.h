#ifndef PLAYINGBOARD_H
#define PLAYINGBOARD_H

#include <QObject>
#include "boardtile.h"

class PlayingBoard : public QObject
{
    Q_OBJECT
public:
    explicit PlayingBoard(QObject *parent = nullptr);
    ~PlayingBoard();
    void setTileValue(int index, BoardTile* newValue);
    BoardTile* getTileValue(int index);
    static BoardTile* getNewTile(BoardTileType type);

signals:

private:
    std::array<BoardTile*, 100> m_board;

};

#endif // PLAYINGBOARD_H
