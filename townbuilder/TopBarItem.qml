import QtQuick 2.0

Rectangle {
    id: barItem
    property alias label: statLabel.text
    property alias icon: statIcon.source
    property alias value: statValue.text
    height: parent.height - 20
    width: (parent.width / 3 ) - 20
    anchors.verticalCenter: parent.verticalCenter


    Image {
        id: statIcon
        height: parent.height - 10
        width: height
        anchors.verticalCenter: parent.verticalCenter
        x:10

    }


    Text {
        id: statLabel
        text: "placeholder"
        x: statIcon.width + 20
        anchors.verticalCenter: parent.verticalCenter
        font {
            bold: true
            pixelSize: 14
        }
    }

    Text {
        id: statValue
        text: value
        x: (statIcon.width + statLabel.x + statLabel.width)
        anchors.verticalCenter: parent.verticalCenter
        font {
            pixelSize: 14
        }
    }



}
