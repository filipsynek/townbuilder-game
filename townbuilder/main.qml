import QtQuick 2.15
import QtQuick.Window 2.15


Window {
    width: 800
    height: 800
    visible: true
    title: qsTr("Townbuilder")


    TopBar {
        id: topBar        

    }

    SideMenu {
        id: sideMenu

    }

    PlayingBoard {
        boardSpacing: 0
    }
}
