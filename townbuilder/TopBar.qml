import QtQuick 2.0

Rectangle {

    property alias itemSpacing: topBarRow.spacing
    width: parent.width
    height: parent.height / 12
    color: "blue"
    anchors.top: parent.top


    Row {
        id: topBarRow
        width: parent.width - (2*itemSpacing)
        height: parent.height - 10
        anchors.centerIn: parent
        spacing: 10

        TopBarItem {
            id: goldPiecesStat
            label: "Zlaťáky"
            icon: "images/gold.png"
            value: qsTr(game.getGoldPieces().toFixed(1))

        }

        TopBarItem {
            id: experiencePointsStat
            label: "Body zkušeností"
            icon: "images/xp.png"
            value: qsTr(game.getExperiencePoints().toString())
        }

        TopBarItem {
            id: populationStat
            label: "Obyvatelé"
            icon: "images/person.png"
            value: qsTr(game.getPopulation().toString())
        }
    }

    Timer{
        interval: 16
        repeat: true
        running: true
        onTriggered: experiencePointsStat.value = game.getExperiencePoints().toString()

    }
    Timer{
        interval: 16
        repeat: true
        running: true
        onTriggered: goldPiecesStat.value = game.getGoldPieces().toFixed(1)

    }
    Timer{
        interval: 16
        repeat: true
        running: true
        onTriggered: populationStat.value = game.getPopulation().toString()

    }
    Timer{
        interval: 1000
        repeat: true
        running: true
        onTriggered: game.addGoldPieces()

    }

}
