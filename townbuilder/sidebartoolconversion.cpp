#include "sidebartoolconversion.h"

SidebarToolConversion::SidebarToolConversion()
{

}

SidebarTool SidebarToolConversion::SidebarToolToEnum(std::string type) {
    if (type == "empty_hands") return SidebarTool::empty_hand;
    if (type == "build_house") return SidebarTool::build_house;
    if (type == "build_church") return SidebarTool::build_church;
    if (type == "build_blacksmith") return SidebarTool::build_blacksmith;
    if (type == "build_townhall") return SidebarTool::build_townhall;
    if (type == "destroy_building") return SidebarTool::destroy_building;
    if (type == "save_file") return SidebarTool::save_file;
    if (type == "load_file") return SidebarTool::load_file;
    if (type == "quit_game") return SidebarTool::quit_game;

    return SidebarTool::empty_hand;
}

QString SidebarToolConversion::SidebarToolToQString(SidebarTool type) {
    switch (type) {
        case SidebarTool::empty_hand:
            return "empty_hand";

        case SidebarTool::build_house:
            return "build_house";

        case SidebarTool::build_church:
            return "build_church";

        case SidebarTool::build_blacksmith:
            return "build_blacksmith";

        case SidebarTool::build_townhall:
            return "build_townhall";

        case SidebarTool::destroy_building:
            return "destroy_building";

        case SidebarTool::save_file:
            return "save_file";

        case SidebarTool::load_file:
            return "load_file";

        case SidebarTool::quit_game:
            return "quit_game";

        default:
            return "empty_hand";

    }
}
