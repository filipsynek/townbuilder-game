#include "boardgame.h"

BoardGame::BoardGame(QObject *parent)
    : QObject{parent}
{
    m_board = FileManager::loadXml();
}

int BoardGame::getBoardCount(int index){
    return m_board.at(index);
}
