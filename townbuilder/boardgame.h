#ifndef BOARDGAME_H
#define BOARDGAME_H

#include <QObject>
#include <vector>
#include "filemanager.h"

class BoardGame : public QObject
{
    Q_OBJECT
public:
    explicit BoardGame(QObject *parent = nullptr);
    Q_INVOKABLE int getBoardCount(int index);

signals:

private:
    std::vector<int> m_board;
};

#endif // BOARDGAME_H
