#include "boardtiletypeconversion.h"

BoardTileTypeConversion::BoardTileTypeConversion(){

}


QString BoardTileTypeConversion::BoardTileTypeToString(BoardTileType type){
    switch (type) {
        case BoardTileType::empty_tile:
            return "empty_tile";
            break;
        case BoardTileType::house:
            return "house";
            break;
        case BoardTileType::blacksmith:
            return "blacksmith";
            break;
        case BoardTileType::forester:
            return "forester";
            break;
        case BoardTileType::church:
            return "church";
            break;
        case BoardTileType::townhall:
            return "townhall";
            break;
        default:
            return "empty_tile";
    }
}

BoardTileType BoardTileTypeConversion::BoardTileTypeToEnum(std::string type){
    if (type == "empty_tile"){
        return BoardTileType::empty_tile;
    }else if (type == "house"){
        return BoardTileType::house;
    }else if (type == "blacksmith"){
        return BoardTileType::blacksmith;
    }else if (type == "forester"){
        return BoardTileType::forester;
    }else if (type == "church"){
        return BoardTileType::church;
    }else if (type == "townhall"){
        return BoardTileType::townhall;
    }
    return BoardTileType::empty_tile;
}
