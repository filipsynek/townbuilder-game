#ifndef BOARDTILE_H
#define BOARDTILE_H

#include <QObject>
#include "boardtiletype.h"

class BoardTile : public QObject
{
    Q_OBJECT
public:
    explicit BoardTile(QObject *parent = nullptr, BoardTileType type = BoardTileType::empty_tile, float additionalGold = 0, int additionalExperience = 0);
    static BoardTile* getNewTile(BoardTileType type);
    BoardTileType getType();
    float getAdditionalGold();
    int getAdditionalExperience();
    QString getTileType();

signals:

private:
    BoardTileType m_type;
    float m_additionalGold;
    int m_additionalExperience;

};

#endif // BOARDTILE_H
