#ifndef SIDEBARTOOLCONVERSION_H
#define SIDEBARTOOLCONVERSION_H

#include <iostream>
#include <QObject>
#include "SidebarTool.h"

class SidebarToolConversion
{
public:
    SidebarToolConversion();

    static SidebarTool SidebarToolToEnum(std::string type);
    static QString SidebarToolToQString(SidebarTool type);
};

#endif // SIDEBARTOOLCONVERSION_H
