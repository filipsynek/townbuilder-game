#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <QXmlStreamReader>
#include <QFile>
#include <QDebug>
#include <vector>
#include "gameengine.h"
#include "boardtiletypeconversion.h"

typedef struct{
    std::array<BoardTileType, 100> board;
    float goldPieces;
    int experiencePoints;
    int population;
} gameData;

class FileManager
{
public:
    FileManager();

    static gameData loadFromFile(QString file_name);
    static void saveToFile(PlayingBoard* board, float goldPieces, int experience, int population);
};

#endif // FILEMANAGER_H
