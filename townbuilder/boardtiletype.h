#ifndef BOARDTILETYPE_H
#define BOARDTILETYPE_H

enum class BoardTileType{
  empty_tile, house, blacksmith, forester, church, townhall
};

#endif // BOARDTILETYPE_H
