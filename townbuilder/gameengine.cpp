#include "gameengine.h"

GameEngine::GameEngine(QObject *parent)
    : QObject{parent}
{
    m_goldPieces = 10;
    m_experiencePoints = 0;
    m_population = 0;
    m_rate = 1;
    m_currentTool = SidebarTool::empty_hand;
    m_board = new PlayingBoard();

}

void GameEngine::loadFromFile(QString file_name){

    m_goldPieces = FileManager::loadFromFile(file_name).goldPieces;
    m_experiencePoints = FileManager::loadFromFile(file_name).experiencePoints;
    m_population = FileManager::loadFromFile(file_name).experiencePoints;
    std::array<BoardTileType, 100> boardFromFile = FileManager::loadFromFile(file_name).board;;
    for (int i = 0; i<100; i++){
        m_board->setTileValue(i, m_board->getNewTile(boardFromFile.at(i)));
    }
}


void GameEngine::saveToFile(){
    FileManager::saveToFile(getPlayingBoard(), getGoldPieces(), getExperiencePoints(), getPopulation());
}


float GameEngine::getGoldPieces(){
    return m_goldPieces;
}


int GameEngine::getPopulation(){
    return m_population;
}

void GameEngine::addPopulation(int position){
    if (m_board->getTileValue(position)->getType() == BoardTileType::house)
        m_population += 10;
    if (m_board->getTileValue(position)->getType() == BoardTileType::church)
        m_population += 20;
    if (m_population >= 1000)
        std::cout<<"Vyhráli jste!"<<std::endl;
}

void GameEngine::decreasePopulation(int position){
    if (m_board->getTileValue(position)->getType() == BoardTileType::house)
        m_population -= 10;
    if (m_board->getTileValue(position)->getType() == BoardTileType::church)
        m_population -= 20;
    if (m_board->getTileValue(position)->getType() == BoardTileType::blacksmith)
        m_rate -= 1;
    if (m_board->getTileValue(position)->getType() == BoardTileType::townhall)
        m_rate -= 3;
}


int GameEngine::getExperiencePoints(){
    return m_experiencePoints;
}


PlayingBoard* GameEngine::getPlayingBoard(){
    return m_board;
}


void GameEngine::addGoldPieces(){
    m_goldPieces += m_rate;
}


void GameEngine::deductGoldPieces(){
    if (getCurrentTool() == SidebarTool::build_house){
        m_goldPieces -= 10;
    }
    if (getCurrentTool() == SidebarTool::build_blacksmith){
        m_goldPieces -= 20;
        m_rate += 1;
    }
    if (getCurrentTool() == SidebarTool::build_church){
        m_goldPieces -= 30;
    }
    if (getCurrentTool() == SidebarTool::build_townhall){
        m_goldPieces -= 50;
        m_rate += 3;
    }
}

bool GameEngine::enoughCredits(){
    if (getCurrentTool() == SidebarTool::build_house){
        if(m_goldPieces >= 10){
            addExperiencePoints(10);
            return true;}
        else {std::cout<<"Nedostatek kreditu"<<std::endl; return false;}
    }
    if (getCurrentTool() == SidebarTool::build_blacksmith){
        if(m_goldPieces >= 20){
            addExperiencePoints(15);
            return true;}
        else {std::cout<<"Nedostatek kreditu"<<std::endl; return false;}
    }
    if (getCurrentTool() == SidebarTool::build_church){
        if(m_goldPieces >= 30){
            addExperiencePoints(20);
            return true;}
        else {std::cout<<"Nedostatek kreditu"<<std::endl; return false;}
    }
    if (getCurrentTool() == SidebarTool::build_townhall){

        if(m_goldPieces >= 50)
            if(m_experiencePoints < 50){
                std::cout<<"Nedostatek zkusenosti"<<std::endl; return false;
            } else{
                addExperiencePoints(50);
                return true;}
        else {std::cout<<"Nedostatek kreditu"<<std::endl; return false;}
    }
    return false;

}


void GameEngine::addExperiencePoints(int ammount){
    m_experiencePoints += ammount;
}


SidebarTool GameEngine::getCurrentTool(){
    return m_currentTool;
}


QString GameEngine::getCurrentToolQString(){
    return SidebarToolConversion::SidebarToolToQString(m_currentTool);
}


void GameEngine::setCurrentTool(SidebarTool newTool){
    m_currentTool = newTool;
}


void GameEngine::setCurrentToolQString(QString tools){
    std::string tool = tools.toStdString();
    setCurrentTool(SidebarToolConversion::SidebarToolToEnum(tool));
    std::cout << (SidebarToolConversion::SidebarToolToQString(getCurrentTool()).toStdString()) <<std::endl;
}


void GameEngine::quitGame(){
    QCoreApplication::quit();
}


void GameEngine::printPos(int pos){
    std::cout<<"Pozice: "<<pos<<std::endl;
}


void GameEngine::setTileState(int pos, QString state){
    m_board->setTileValue(pos, m_board->getNewTile(BoardTileTypeConversion::BoardTileTypeToEnum(state.toStdString())));

}


QString GameEngine::getTileState(int pos){
    return m_board->getTileValue(pos)->getTileType();

}



