#ifndef SIDEBARTOOL_H
#define SIDEBARTOOL_H

enum class SidebarTool {
    empty_hand,
    build_house,
    build_church,
    build_blacksmith,
    build_townhall,
    destroy_building,
    save_file,
    load_file,
    quit_game,
};


#endif // SIDEBARTOOL_H

